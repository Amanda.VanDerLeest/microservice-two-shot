import { Link } from 'react-router-dom';

function MainPage() {
  return (
    <>
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">WARDROBIFY!</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          Need to keep track of your shoes and hats? We have
          the solution for you!
        </p>
      </div>
    </div>
    <div className="btn d-grid gap-2 col-6 mx-auto">
        <Link to="/hats/" className="btn btn-outline-dark btn-lg px-4 gap-3">HATS</Link>
    </div>
    <div className="btn d-grid gap-2 col-6 mx-auto">
        <Link to="/shoes/" className="btn btn-outline-dark btn-lg px-4 gap-3">SHOES</Link>
    </div>
    </>
  );
}

export default MainPage;
