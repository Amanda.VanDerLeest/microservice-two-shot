import React from "react";

class BinList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      bins: [],
    };
  }

  async componentDidMount() {
    const url = 'http://localhost:8080/api/shoes/bins/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      const bins = data.bins;
      this.setState({ bins: bins });
    }
  }

  render() {
    return (
      <>
        <h1 className="mt-4">ALL THE SHOES</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Closet name</th>
              <th>Bin number</th>
              <th>Bin size</th>
            </tr>
          </thead>
          <tbody>
            {this.state.bins.map(bin => {
              return (
                <tr key={bin.href}>
                  <td>{bin.closet_name}</td>
                  <td>{bin.bin_number}</td>
                  <td>{bin.bin_size}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </>
    );
  }
}

export default BinList;
