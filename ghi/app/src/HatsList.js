import React from "react";
import { Link } from 'react-router-dom';


// added this code in to handle the delete functionality
function handleRemove (id) {
  fetch(`http://localhost:8090/api/hats/${id}`,{method:"DELETE"})
    .then(async response => {
        const data = await response.json();

        if (!response.ok) {
            // get error message from body or default to response status
            const error = (data && data.message) || response.status;
            return Promise.reject(error);
        }

        console.log("great success!")
        const element = document.getElementById(`hat_${id}`);
        element.remove();
    })
    .catch(error => {
        console.error('There was an error!', error);
    });
}
// end of delete block, but look at line 33 to see where it is being called in the code


function HatColumn(props) {
  return(
    <div className="col">
      {props.list.map(hat => {
        return (
          <div key={hat.id} id={`hat_${hat.id}`} className="card mb-3 shadow">
            <img src={hat.picture_url} className="card-img-top"/>
            <div className="card-body">
              <h5 className="card-title">{hat.style_name}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                {hat.color}
              </h6>
              <h6 className="card-subtitle mb-2 text-muted">
                {hat.fabric}
              </h6>
            </div>
            <div className="card-footer">
              Closet Name: {hat.location.closet_name} <br/>
              Section Number: {hat.location.section_number}<br/>
              Shelf Number: {hat.location.shelf_number}
            </div>
            <button type="button" onClick={() => handleRemove(hat.id)}> Delete </button>
          </div>
        );
      })}
    </div>
  );
}

class HatsList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hatColumns: [[], [], []],
    };
  }

  async componentDidMount() {
    const url = 'http://localhost:8090/api/hats/';
// this URL was for getting data for the list of hats 

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        
        const requests = [];
        for (let hat of data.hats) {
          const detailUrl = `http://localhost:8090/api/hats/${hat.id}`;
          requests.push(fetch(detailUrl));
        }

        const responses = await Promise.all(requests);

        const hatColumns = [[], [], []];

        let i= 0;
        for (const hatResponse of responses) {
          if (hatResponse.ok) {
            const details = await hatResponse.json();
            hatColumns[i].push(details);
            i = i + 1;
            if (i > 2) {
              i = 0;
            }
          } else {
            console.error(hatResponse);
          }
        }
        this.setState({hatColumns: hatColumns});
      }
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    return (
      <>
        <figure className="text-end">
          <blockquote className="blockquote">
            <p>How a hat makes you feel is what a hat is all about.</p>
          </blockquote>
          <figcaption className="blockquote-footer">
            <cite title="Source Title">Philip Treacy</cite>
          </figcaption>
        </figure>
        <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
          <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
          <h1 className="display-5 fw-bold">Locate a Hat</h1>
          <h1 className="display-8 fw">or</h1>
          <div className="col-lg-6 mx-auto">
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
              <Link to="/hats/new" className="btn btn-outline-dark btn-lg px-4 gap-3">Add a new hat</Link>
            </div>
          </div>
        </div>
        <div className="container">
          <h2 className="text-center">Hats</h2>
          <div className="row">
            {this.state.hatColumns.map((hatList, index) => {
              return (
                <HatColumn key={index} list={hatList}/>
              );
            })}
          </div>
        </div>
      </>
    );
  }
}

export default HatsList;
