from django.http.response import JsonResponse
import json
from common.json import ModelEncoder
from .models import BinVO, Shoe
from django.views.decorators.http import require_http_methods


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["id", "closet_name", "bin_number", "bin_size"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer", "model_name", "color", "id"]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer", "model_name", "color", "picture_url", "bin", "id"]
    encoders = {"bin": BinVOEncoder(),}


@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin = bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder = ShoeListEncoder,
        )

    else:
        content = json.loads(request.body)

        try:
            if "bin" in content:
                bin_id = content["bin"] 
                bin = BinVO.objects.get(id = bin_id)
                content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status = 400,
            )
        
        shoe = Shoe.objects.create(**content)

        return JsonResponse (
            shoe,
            encoder = ShoeDetailEncoder,
            safe = False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder = ShoeDetailEncoder,
            safe = False,
        )
    elif request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder = ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status = 404,
            )
    else:
        try:
            content = json.loads(request.body)
            shoe = Shoe.objects.get(id=pk)
            props = ["manufacturer", "model_name", "color", "picture_url", "bin"]
            for prop in props:
                if prop in content:
                    setattr(shoe, prop, content[prop])
            shoe.save()
            return JsonResponse(
                shoe,
                encoder = ShoeDetailEncoder,
                safe = False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
        



















# CURTIS CODE?
# class BinVOEncoder(ModelEncoder):
#     model = BinVO
#     properties = [
#         "id",
#         "closet_name",
#         "bin_number",
#         "bin_size",
#     ]

# def api_bins(request):
#     if request.method == "GET":
#         bins = BinVO.objects.all()
#         return JsonResponse(
#             {"bins": bins},
#             encoder=BinVOEncoder,
#         )
#     elif request.method == "POST":
#         data = json.loads(request.body)
#         bin = BinVO.objects.create(**data)
#         return JsonResponse(
#             bin,
#             encoder=BinVOEncoder,
#             safe=False,
#         )
