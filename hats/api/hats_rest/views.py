from django.shortcuts import render

from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Hat, LocationVO
import json
from django.views.decorators.http import require_http_methods

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "id", "shelf_number", "section_number"]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["style_name", "color", "id"]

class HatDetailEncoder(ModelEncoder): 
    model = Hat
    properties = ["fabric", "style_name", "color", "picture_url", "location", "id"]
    encoders = {
        "location": LocationVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location = location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder = HatListEncoder,
        )
        
    else:
        content = json.loads(request.body)

        try:
            if "location" in content:
                location_id = content["location"]
                location = LocationVO.objects.get(id = location_id)
                content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status = 400,
            )

        hat = Hat.objects.create(**content)

        return JsonResponse(
            hat,
            encoder=HatDetailEncoder, 
            safe=False, 
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_hat(request, pk): 
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder= HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder = HatDetailEncoder,
                safe = False,
            )
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status = 404,
            )
    else:
        try:
            content = json.loads(request.body)
            hat = Hat.objects.get(id=pk)
            props = ["fabric", "style_name", "color", "picture_url"]
            for prop in props:
                if prop in content:
                    setattr(hat, prop, content[prop])
            hat.save()
            return JsonResponse(
                hat,
                encoder = HatDetailEncoder,
                safe = False,
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    
