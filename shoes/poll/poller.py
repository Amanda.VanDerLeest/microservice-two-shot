import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from hats_rest, here.
from shoes_rest.models import BinVO

def get_bins():
    response = requests.get("http://wardrobe-api:8000/api/bins/")
    content = json.loads(response.content)
    print("checking for AMANDA SHOES", content)
    for bin in content ["bins"]:
        BinVO.objects.update_or_create(
            id=bin["id"],
            defaults = {"closet_name": bin["closet_name"],
            "bin_number": bin["bin_number"],
            "bin_size": bin["bin_size"]},
        )
    

def poll():
    while True:
        print('Bins poller polling for data')
        try:
            get_bins()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(10)
    pass


if __name__ == "__main__":
    poll()







# Curtis Code Below:
# def poll():
#     while True:
#         print('Shoes poller polling for data')
#         try:
#             response = requests.get("http://wardrobe-api:8000/api/bins/")
#             data = response.json()
#             for bin in data["bins"]:
#                 bin_vo, created = BinVO.objects.update_or_create(
#                     href=bin["href"],
#                     defaults={
#                         "closet_name": bin["closet_name"],
#                         "bin_number": bin["bin_number"],
#                         "bin_size": bin["bin_size"],
#                     }
#                 )
#                 print("Created?", created, ":", bin_vo)
#         except Exception as e:
#             print(e, file=sys.stderr)
#         time.sleep(60)


# if __name__ == "__main__":
#     poll()
