import React from "react";
import { Link } from 'react-router-dom';


// #5
function handleRemove (id) {
    fetch(`http://localhost:8080/api/shoes/${id}`,{method:"DELETE"})
      .then(async response => {
          const data = await response.json();
  
          if (!response.ok) {
              // get error message from body or default to response status
              const error = (data && data.message) || response.status;
              return Promise.reject(error);
          }
  
          console.log("great success!")
          const element = document.getElementById(`shoe_${id}`);
          element.remove();
      })
      .catch(error => {
          console.error('There was an error!', error);
      });
  }

// #4
function ShoeColumn(props) {
    return(
      <div className="col">
        {props.list.map(shoe => {
          return (
            <div key={shoe.id} id={`shoe_${shoe.id}`} className="card mb-3 shadow">
              <img src={shoe.picture_url} className="card-img-top"/>
              <div className="card-body">
                <h5 className="card-title">{shoe.model_name}</h5>
                <h6 className="card-subtitle mb-2 text-muted">
                  {shoe.manufacturer}
                </h6>
                <h6 className="card-subtitle mb-2 text-muted">
                  {shoe.color}
                </h6>
              </div>
              <div className="card-footer">
                Closet Name: {shoe.bin.closet_name} <br/>
                Bin Number: {shoe.bin.bin_number}<br/>
                Bin Size: {shoe.bin.bin_size}
              </div>
              <button type="button" onClick={() => handleRemove(shoe.id)}> Delete </button>
            </div>
          );
        })}
      </div>
    );
  }

// #1 start with making the class
class ShoesList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            shoeColumns: [[], [], []],
        };
    }

    // #2
    async componentDidMount() {
    const url = 'http://localhost:8080/api/shoes/';
        // this URL is for getting data for the list of hats - you can find it in insomnia

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();

                const requests = [];
                for (let shoe of data.shoes) {
                    const detailUrl = `http://localhost:8080/api/shoes/${shoe.id}`;
                    requests.push(fetch(detailUrl));
                }

                const responses = await Promise.all(requests);

                const shoeColumns = [[], [], []];

                let i=0;
                for (const shoeResponse of responses) {
                    if (shoeResponse.ok) {
                        const details = await shoeResponse.json();
                        shoeColumns[i].push(details);
                        i = i + 1;
                        if (i > 2) {
                            i = 0;
                        }
                    } else {
                        console.error(shoeResponse);
                    }
                }
                this.setState({shoeColumns: shoeColumns});
            }
            } catch (e) {
            console.error(e);
        }
    }

    // #3
    render(){
        return (
            <>
              <figure className="text-end">
                <blockquote className="blockquote">
                  <p>If I ever let my head down, it will be to admire my shoes.</p>
                </blockquote>
                <figcaption className="blockquote-footer">
                  <cite title="Source Title">Marilyn Monroe</cite>
                </figcaption>
              </figure>
              <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
                <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
                <h1 className="display-5 fw-bold">Locate Your Shoes</h1>
                <h1 className="display-8 fw">or</h1>
                <div className="col-lg-6 mx-auto">
                  <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                    <Link to="/shoes/new" className="btn btn-outline-dark btn-lg px-4 gap-3">Add New Shoes</Link>
                  </div>
                </div>
              </div>
              <div className="container">
                <h2 className="text-center">Shoes</h2>
                <div className="row">
                  {this.state.shoeColumns.map((shoeList, index) => {
                    return (
                      <ShoeColumn key={index} list={shoeList}/>
                    );
                  })}
                </div>
              </div>
            </>
          );
    }
}

export default ShoesList;