import React from 'react';

class HatForm extends React.Component {
  constructor (props) {
    super(props);
      this.state = {
        style_name:'',
        color: '',
        fabric: '',
        picture_url:'',
        location: '',
        locations: [],
        hasSignedUp: false,
      };

      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleChangeStyleName = this.handleChangeStyleName.bind(this);
      this.handleChangeColor = this.handleChangeColor.bind(this);
      this.handleChangeFabric = this.handleChangeFabric.bind(this);
      this.handleChangePictureUrl = this.handleChangePictureUrl.bind(this);
      this.handleChangeLocation = this.handleChangeLocation.bind(this);
  }


// this one is pulling in the locations for choose a location select tag
  async componentDidMount() {
    const url = 'http://localhost:8100/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ locations: data.locations });
    }
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    delete data.locations;
    delete data.hasSignedUp;

    // what are you posting the new item to in insomnia? That's the URL here
    const hatUrl = 'http://localhost:8090/api/hats/'
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
    };
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
        const newHat = await response.json();
        console.log(newHat);
        this.setState({
            style_name:'',
            color: '',
            fabric: '',
            picture_url:'',
            location: '',
            hasSignedUp: true,
        });
    }
  }

  handleChangeStyleName(event) {
    const value = event.target.value;
    this.setState({ style_name: value});
  }

  handleChangeColor(event) {
    const value = event.target.value;
    this.setState({ color: value});
  } 

  handleChangeFabric(event) {
    const value = event.target.value;
    this.setState({ fabric: value});
  }

  handleChangePictureUrl(event) {
    const value = event.target.value;
    this.setState({ picture_url: value});
  }

  handleChangeLocation(event) {
    const value = event.target.value;
    this.setState({ location: value});
  }



  render() {

    
    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (this.state.hasSignedUp) {
      messageClasses = 'alert alert-success mb-0';
      formClasses = 'd-none';
    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a new hat</h1>
            <form className={formClasses} onSubmit={this.handleSubmit} id="create-hat-form">

              <div className="form-floating mb-3">
                <input onChange={this.handleChangeStyleName} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control" />
                <label htmlFor="style_name">Style Name</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={this.handleChangeColor} placeholder="Hat Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Hat Color</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={this.handleChangeFabric} placeholder="Hat Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                <label htmlFor="fabric">Hat Fabric</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={this.handleChangePictureUrl} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">Picture Url</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleChangeLocation} required name="location" id="location" className="form-select">
                  <option value="">Choose a location</option>
                  {this.state.locations.map(location => {
                    return (
                      <option key={location.id} value={location.id}>{location.closet_name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
            <br/>
            <div className={messageClasses} id="success-message">
              You've added a new hat!
            </div>
          </div>
        </div>
      </div>
    );
  }
}


export default HatForm;