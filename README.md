# Wardrobify

Solo:

* Amanda V., hats first (finished), shoes second

## Design

## Hats microservice (done)

Explain your models and integration with the wardrobe
microservice, here.

- Make a MODEL and have it have properties of a fabric, style name, color and include a URL for a picture and the location in the wardrobe where it exists.

- Create a RESTFUL APIs to get the
    - list of hats & the details (used cards)
    - create a new hat
    - delete a hat

- Create a react component for:
    -showing a list of all hats and their details
    -showing a form to create a new hat

- You must provide a way to delete a hat

- Must route the existing navigation links to your components



## Shoes microservice (in progress)

Explain your models and integration with the wardrobe
microservice, here.

- Make a Shoe MODEL and have it have properties of a manufacturer, model name, color and include a URL for a picture and the bin in the wardrobe where it exists

- Create a RESTFUL APIs to get the
    - list of shoes & the details (using cards)
    - create a new shoe
    - delete a shoe

- Create a react component for:
    - showing a list of all shoes, details and delete button on cards
    - showing a form to create a new hat
    
- Must route the existing navigation links to your components

## ports for shoes:
    see data in database: http://localhost:8080/api/shoes/
    webpage: http://localhost:3000/shoes

    wardrobe locations for  API & insomnia: http://localhost:8100/api/locations/ 

    existing wardrobe: http://wardrobe-api:8000